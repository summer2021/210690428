import  sys
import time

sys.path.insert(0,"..")

from opcua import Client

if __name__=="__main__":
    client = Client("opc.tcp://0.0.0.0:4840/freeopcua/server/")
    client.set_security_string("Basic256Sha256,SignAndEncrypt,certClient.pem,keyClient.pem")

    try:
        client.connect()
        client.load_type_definitions()

        root = client.get_root_node()
        print("Object node is: ", root)
        print("Children of root are:", root.get_children())
        var = client.get_node("ns=2;i=2")
        print(var)
        value = var.get_value()
        print(value)
        switch = client.get_node("ns=2;i=3")
        print(switch.get_value())
        #switch.set_value(False)

        obj = root.get_child(["0:Objects","2:device"])
        print("MyObject is:", obj)


    finally:
        client.disconnect()