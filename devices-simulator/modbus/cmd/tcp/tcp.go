package tcp

import (
	"github.com/spf13/cobra"
	"tests/devices-simulator/modbus/cmd/tcp/config"
	"tests/devices-simulator/modbus/device"
)

func CommandTCP() *cobra.Command {
	var cfg = config.NewConfig()

	var c = &cobra.Command{
		Use:  "tcp",
		Long: "modbus tcp simulator",
		RunE: func(cmd *cobra.Command, args []string) error {
			return device.RunAsTCPNormal(cfg)
		},
	}

	cfg.Flags(c.Flags())
	return c
}
