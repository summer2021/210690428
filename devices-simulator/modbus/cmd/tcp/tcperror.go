package tcp

import (
	"github.com/spf13/cobra"
	"tests/devices-simulator/modbus/cmd/tcp/config"
	"tests/devices-simulator/modbus/device"
)

func CommandTCPError() *cobra.Command {
	var cfg = config.NewConfig()

	var c = &cobra.Command{
		Use:  "tcperror",
		Long: "modbus tcp simulator",
		RunE: func(cmd *cobra.Command, args []string) error {
			return device.RunAsTCPError(cfg)
		},
	}

	cfg.Flags(c.Flags())
	return c
}
