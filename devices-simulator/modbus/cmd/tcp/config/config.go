package config

import (
	"github.com/spf13/pflag"
)

type Config struct {
	SlaveID  uint8
	Port     int
	Interval int
}

func (cfg *Config) Flags(fs *pflag.FlagSet) {
	fs.Uint8VarP(&cfg.SlaveID, "id", "", cfg.SlaveID, "")
	fs.IntVarP(&cfg.Port, "port", "p", cfg.Port, "")
	fs.IntVarP(&cfg.Interval, "interval", "i", cfg.Interval, "")
}

func NewConfig() *Config {
	return &Config{
		SlaveID:  1,
		Port:     5020,
		Interval: 60,
	}
}
