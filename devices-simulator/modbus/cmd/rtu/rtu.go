package rtu

import (
	"github.com/spf13/cobra"
	"tests/devices-simulator/modbus/cmd/rtu/config"
	"tests/devices-simulator/modbus/device"
)

func CommandRTU() *cobra.Command {
	cfg := config.NewConfig()

	c := &cobra.Command{
		Use:  "rtu",
		Long: "modbus rtu simulator",
		RunE: func(cmd *cobra.Command, args []string) error {
			return device.RunAsRTUNormal(cfg)
		},
	}

	cfg.Flags(c.Flags())
	return c

}
