package rtu

import (
	"github.com/spf13/cobra"
	"tests/devices-simulator/modbus/cmd/rtu/config"
	"tests/devices-simulator/modbus/device"
)

func CommandRTUEroor() *cobra.Command {
	var cfg = config.NewConfig()

	var c = &cobra.Command{
		Use:  "rtuerror",
		Long: "modbus rtu error simulator  ",
		RunE: func(cmd *cobra.Command, args []string) error {
			return device.RunAsRTUError(cfg.Normalize())
		},
	}
	cfg.Flags(c.Flags())
	return c
}
