package main

import (
	"os"
	"path/filepath"
	"tests/devices-simulator/modbus/cmd/rtu"
	"tests/devices-simulator/modbus/cmd/tcp"

	"github.com/spf13/cobra"
)

var allCommands = []*cobra.Command{
	tcp.CommandTCP(),
	rtu.CommandRTU(),
	tcp.CommandTCPError(),
	rtu.CommandRTUEroor(),
}

func main() {
	var c = &cobra.Command{
		Use: "modbus",
		RunE: func(cmd *cobra.Command, args []string) error {
			var (
				basename  = filepath.Base(os.Args[1])
				targetCmd *cobra.Command
			)
			for _, cmd := range allCommands {
				if cmd.Name() == basename {
					targetCmd = cmd
					break
				}
			}
			if targetCmd != nil {
				return targetCmd.Execute()
			}
			return cmd.Help()
		},
	}
	c.AddCommand(allCommands...)

	if err := c.Execute(); err != nil {
		os.Exit(1)
	}
}
