package device

import (
	"github.com/tbrandon/mbserver"
	"time"
)

const readDiscreteRegisterCode = 2
const writeHoldingRegisterCode = 6

func ReadDiscreteRegisterError(s *mbserver.Server, frame mbserver.Framer) ([]byte, *mbserver.Exception) {
	return frame.GetData()[0:4], &mbserver.SlaveDeviceFailure
}

func WriteHoldingRegisterError(s *mbserver.Server, frame mbserver.Framer) ([]byte, *mbserver.Exception) {
	time.Sleep(10 * time.Second)
	return []byte{}, &mbserver.Success
}

func InsertServerError(s *mbserver.Server) {
	s.RegisterFunctionHandler(readDiscreteRegisterCode, ReadDiscreteRegisterError)
	s.RegisterFunctionHandler(writeHoldingRegisterCode, WriteHoldingRegisterError)
}
