module tests/devices-simulator/modbus

go 1.15

require (
	github.com/goburrow/modbus v0.1.0
	github.com/goburrow/serial v0.1.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.2.1
	github.com/spf13/pflag v1.0.5
	github.com/tbrandon/mbserver v0.0.0-20210320091329-a1f8ae952881
	k8s.io/klog v1.0.0
)
