# mapper e2e test

### modbus simulator

Modbus simulator is mocking a thermometer, the numerical accuracy is two decimal places, and the measurement is celsius temperature and relative humidity.

It can run as  modbus tcp or modbus rtu protocol.

when it run as modbus rtu protocol device, should download socat image , socat can simulate COM.



| Name                   | RegisterType    | Type    | Property   | Address | Quantity | Value                                                        |
| ---------------------- | --------------- | ------- | ---------- | ------- | -------- | ------------------------------------------------------------ |
| Switch                 | CoilRegister    | boolean | read/write | 0       | 1        | default value is true                                        |
| Temperature            | HoldingRegister | Float64 | read       | 0       | 4        | the random value range is range -99-99                       |
| alarming temperature   | HoldingRegister | Int16   | read/write | 4       | 1        | High temperature threshold default value is 20               |
| High Temperature Alarm | CoilRegister    | boolean | read       | 1       | 1        | over temperature warning                                     |
| Humidity               | HoldingRegister | int32   | read       | 5       | 2        | the random value  range is 5-95%                             |
| alarming humidity      | HoldingRegister | Int32   | read/write | 7       | 2        | High humudity  threshold default value is 60                 |
| High Humidity Alarm    | DiscretRegister | boolean | read       | 0       | 1        | over humidity warning                                        |
| Battery                | HoldingRegister | Int32   | read       | 9       | 2        | Battery value, the default value is 100%, minus 1% every 2 minutes. |
| Temperature Accuracy   | InputRegister   | Float32 | read       | 0       | 2        | the measurement accuracy is 0.5                              |
| Humidity Accuracy      | InputRegister   | Int8    | read       | 2       | 1        | the measurement accuracy is 3                                |
| Device name            | InputRegister   | string  | read       | 3       | 12       | Huawei modbus simulator                                      |

##### modbus error device

**when it run as error device, it add two error.  when you read discrete register,  it will response a slavedevicefailure error code. when you write holding register, it will response timeout.**

### opcua simulator

| Name                  | Type   | Property   | NodeID   | Value                  |
| --------------------- | ------ | ---------- | -------- | ---------------------- |
| switch                | bool   | read/write | ns=2;i=2 | default true           |
| temperature           | float  | read       | ns=2;i=3 | the random value range |
| humidity              | float  | read       | ns=2;i=4 | the random value range |
| temperature threshold | int    | read/write | ns=2;i-5 | Default value 40       |
| humidity threshold    | Int    | read/write | ns=2;i=6 | Default value 20       |
| device name           | string | read       | ns=2;i=7 | Huawei opcua simulator |



### how to start e2e

* modbus

  ```
  cd ./tests/e2e
  make e2e modbus test
  ```

* opcua

  ```
  cd ./tests/e2e
  make e2e opcua test
  ```



