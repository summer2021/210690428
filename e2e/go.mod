module tests/e2e

go 1.15

require (
	github.com/onsi/ginkgo v1.16.4
	github.com/onsi/gomega v1.15.0
	k8s.io/klog v1.0.0
)
