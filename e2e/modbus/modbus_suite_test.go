package modbus_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	log "k8s.io/klog"
	"testing"
	"tests/e2e/modbus/constants"
	"tests/e2e/utils"
)

func TestModbus(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Modbus Suite")
}

var _ = BeforeSuite(func() {
	log.Info("Before Suiting Execution")
	err := utils.MakeMapperImages(constants.MakeModbusMapper, constants.CheckModbusMapperImage)
	if err != nil {
		log.Error("Fail to run make mapper image")
		Expect(err).Should(BeNil())
	}
	err = utils.MakeDeviceImage(constants.MakeModbusDevice, constants.CheckModbusDeviceImage)
	if err != nil {
		log.Error("Fail to make device image")
		Expect(err).Should(BeNil())
	}
})

var _ = AfterSuite(func() {
	log.Info("After Suiting Executing")
	err := utils.DeleteMapperImage(constants.DeleteModbusMapperImage)
	if err != nil {
		log.Error("Fail to delete mapper image")
		Expect(err).Should(BeNil())
	}
	err = utils.DeleteDeviceImage(constants.DeleteModbusDevice)
	if err != nil {
		log.Error("Fail to delete device image")
		Expect(err).Should(BeNil())
	}
})
