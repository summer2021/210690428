package utils

import (
	"os/exec"
)

func CopyDirectory(srcFileName, dstFileName string) error {
	cmd := exec.Command("sh", "-c", "cp -r "+srcFileName+" "+dstFileName)
	if err := PrintCmdOutput(cmd); err != nil {
		return err
	}
	return nil
}

func RmDirectory(fileName string) error {
	cmd := exec.Command("sh", "-c", "rm -r "+fileName)
	if err := PrintCmdOutput(cmd); err != nil {
		return err
	}
	return nil
}
