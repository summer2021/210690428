package utils

import (
	log "k8s.io/klog"
	"os/exec"
)

func MakeDeviceImage(makeDevice string, checkDeviceImage string) error {
	cmd := exec.Command("sh", "-c", makeDevice)
	if err := PrintCmdOutput(cmd); err != nil {
		return err
	}
	cmd = exec.Command("sh", "-c", checkDeviceImage)
	if err := PrintCmdOutput(cmd); err != nil {
		return err
	}
	return nil
}

func DeleteDeviceImage(deleteDeviceImage string) error {
	cmd := exec.Command("sh", "-c", deleteDeviceImage)
	if err := PrintCmdOutput(cmd); err != nil {
		return err
	}
	return nil
}

// run device container
func StartDevices(runDeviceContainer string, checkDeviceRun string) error {
	cmd := exec.Command("sh", "-c", runDeviceContainer)
	if err := PrintCmdOutput(cmd); err != nil {
		return err
	}

	// confirm whether the device can start normally
	cmd = exec.Command("sh", "-c", checkDeviceRun)
	if err := PrintCmdOutput(cmd); err != nil {
		return err
	}
	return nil
}

//stop mapper container
func StopAndDeleteDevice(getContainerID string) error {
	log.Info("stop device running")
	cmd := exec.Command("sh", "-c", getContainerID)
	result, err := cmd.CombinedOutput()
	if err != nil {
		return err
	}
	containerID := string(result[:12])

	log.Info("stop and delete device container")
	cmd = exec.Command("sh", "-c", "docker stop "+containerID)
	if err = PrintCmdOutput(cmd); err != nil {
		return err
	}
	cmd = exec.Command("sh", "-c", "docker rm "+containerID)
	if err = PrintCmdOutput(cmd); err != nil {
		return err
	}
	return nil
}
