# mapper e2e design 

## 1. Introduction

### 1.1 Purpose

Add end-to-end test cases for mapper to ensure the correct running.

Currently completed the opcua and modbus mapper tests.

## 2. e2e process introduction

**running a test case  general process**

According to different test case, use different deviceprofiles. 

1. build mapper images. (mapper run as container during e2e test.)
2. build device images. (Device run as container during e2e test)
3. Running device container. (Network is host)
4. running mapper container. Before running mapper, it should prepare deviceprofiles and mount this. (network is host)
5. After running some seconds, read container and device log to determine whether the test case is correct.
6. After a test case finished.  Stop and delete device and mapper container.
7. Delete  mapper and device images

note : opcua mapper should mount ca file path.

### 3. test case

* modbus

  | test case description                             |          |
  | ------------------------------------------------- | -------- |
  | Reading holding register  value  - float64        | Finished |
  | Reading holding register  value  - float64  (rtu) | Finished |
  | Reading coil Register                             | /        |
  | Reading discrete register                         | /        |
  | Reading input register                            | /        |
  | Reading input register - string                   | /        |
  | Write data into coil Register                     | /        |
  | Write data into holding register-int16            | Finished |

* modbus negative case 

  | Negative case desciption             |          |
  | ------------------------------------ | -------- |
  | request device returns an error code | finished |
  | request device time out              | finished |

* opcua

  | Test case description               |          |
  | ----------------------------------- | -------- |
  | Reading int data                    | /        |
  | Reading bool data                   | /        |
  | Reading float data                  | Finished |
  | write bool data                     | /        |
  | Write int data                      | Finished |
  | Access method：certifications       | /        |
  | Access method： username - password | /        |
  | Access method： none                | /        |

* opcua negativce case

  | Negative case description          |          |
  | ---------------------------------- | -------- |
  | False certifications               | /        |
  | false Password                     | /        |
  | Write data on read-only attributes | /        |
  | device stop running                | finished |

  